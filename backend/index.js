import 'dotenv/config.js'
import express from 'express';
import passport from 'passport'
import GoogleStrategy from 'passport-google-oauth2';


const app = express();
const port = 3000;
//ALOJA LA INFORMACION DE FORMA TEMPORAL DE LOS USUARIOS CONECTADOS
let docum = [];
let url_frontEnd = 'https://myzero.loca.lt';



app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


app.use(express.urlencoded({ extended: true }));
app.use(express.json());


passport.serializeUser((user, cb) => {
    cb(null, user)
})

passport.deserializeUser((user, cb) => {
    cb(null, user)
})

passport.use(new GoogleStrategy({
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: "https://mytokensito.loca.lt/auth/google/mytokensito",
    passReqToCallback: true
},
    function (request, accessToken, refreshToken, profile, done) {
        docum.push(profile)
        return done(null, profile);
    }
));



app.get("/dataGoogle/:id", (req, res) => {
    //BUSCA EL URUARIO CUYO GITLAB CODE CORRESPONDE
    let aux = docum.filter(arr => arr.id == req.params.id);
    // Y LO ELIMINA DEL ARR
    let index = docum.indexOf(aux[0]);
    docum.splice(index, 1)
    res.json(aux);

});



app.get('/auth/google/failure', (req, res) => {
    res.send('fail')
});

app.get('/auth/google',
    passport.authenticate('google', {
        scope:
            ['email', 'profile']
    }
    ));

app.get('/auth/google/mytokensito',
    passport.authenticate('google', {
        failureRedirect: '/auth/google/failure'
    }),(req,res)=>{
        let aux = docum.filter(arr => arr.id == req.user.id);
        res.redirect(`${url_frontEnd}/google/${req.user.id}`)
    });

app.listen(port, () => {
    console.log(`Server on in port ${port}`);
});

